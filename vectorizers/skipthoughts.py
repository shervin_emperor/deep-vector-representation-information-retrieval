from helpers import is_subscriptable as _is_subscriptable
from itertools import islice as _islice
from . import Base as _Base, _compression as _compression
import sys as _sys
import msgpack as _msgpack
import gzip as _gzip

__all__ = ['init',
		   'Vectorizer',
		   'ChunkVectorizer']

def init(modulePath):
	global _skipthoughtsl
	global _skip_model
	global _skip_encoder
	_sys.path.insert(0, modulePath)
	import skipthoughts
	_skipthoughtsl = skipthoughts
	_skip_model = _skipthoughtsl.load_model()
	_skip_encoder = _skipthoughtsl.Encoder(_skip_model)


class Vectorizer(_Base):
    def __init__(self, verbose=False):
        self.verbose = verbose
        
    def vecfn(self, X):
        return _skip_encoder.encode(X, verbose=self.verbose)
    
    name = 'Skip'
    seperator = '.'


class ChunkVectorizer(Vectorizer):
    def __init__(self, batch_size=32768, verbose=False):
        self.batch_size = batch_size
        super().__init__(verbose)
    
    def vectorize(self, X, save_dir=None):
        def getRange(n, batch_size):
            for i in range(n / batch_size):
                 yield (i * batch_size, (i+1) * batch_size)
        
        def push(X):
            v = self.vecfn(X)
            f.write(packer.pack(v))
        
        if _is_subscriptable(X):
            g = ( lambda: (X[beg:end] for beg, end in getRange(len(X), self.batch_size)) )
        else:
            def g():
                t = iter(X)
                while(True):
                    l = list(_islice(t, self.batch_size))
                    if len(l) == 0:
                        break
                    yield l
        
        addr = (save_dir or './') + '{}{}'.format(self.__class__.name, self.batch_size) + '.pkg.gzip'
        with _gzip.open(addr, 'wb', _compression) as f:
            packer = _msgpack.Packer(use_bin_type=True)
            for x in g():
                push(x)
        
        return self.load(save_dir)
    
    @classmethod
    def load(cls, diraddr=None):
        addr = (diraddr or './') + '{}'.format(cls.name) + '.pkg.gzip'
        open(addr).close()
        def serve():
            with gzip.open(addr, 'rb', _compression) as f:
                unpacker = _msgpack.Unpacker(f, use_list=False)
                for b in unpacker:
                    yield from b
        
        return list(serve())