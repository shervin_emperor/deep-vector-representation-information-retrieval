import regex as re
import msgpack
import gzip
from abc import ABC, abstractmethod

compression = 7

class parser(ABC):
    """
    Abstract parser class, needs to implement method handleCapture(self, instance)
    """
    def __init__(self, regex, encoding=None):
        self.regex = regex
        self.encoding = encoding
        super().__init__()
    
    @abstractmethod
    def handleCapture(self, instance):
        "abstract function"
        pass
    
    @abstractmethod
    def getResult(self):
        "abstract function"
        pass
    
    def feed(self, text):
        for c in re.finditer(self.regex, text):
            self.handleCapture(c)
        return self
    
    def feedFile(self, path, bufferSize = 255):
        buffer = open(path, buffering= bufferSize, encoding=self.encoding).read()
        return self.feed(buffer)
    
    def dump(self, file=None):
        if file is None:
            cls = self.__class__
            file = cls.__name__ if cls.filename is None else cls.filename
        msgpack.pack(self.getResult(), gzip.open(file + '.pkg.gzip', 'wb', compression), use_bin_type=True)
        return self
    
    @classmethod
    def load(cls, file=None):
        if file is None:
            file = cls.__name__ if cls.filename is None else cls.filename
        return msgpack.unpack(gzip.open(file + '.pkg.gzip', 'rb', compression), use_list=False, encoding='utf-8')


class DocParser(parser):
    """
    Parameters
    ----------
    preprocess : if a function is provided, it'll be applied to the text
    """
    
    filename = 'docs'
    regex = '<DOCNO>\s*([^<]+?)\s*</DOCNO>([\s\S]+?)<TEXT>([^<]+?)</TEXT>'
    
    class SubParser(parser):
        regex = '<([^>]+)>\s*((?|\s*[^<\s])+)\s*</\\1>'
        def __init__(self, encoding=None):
            super().__init__(self.__class__.regex, encoding)
        
        def handleCapture(self, c):
            tag, body = c.groups()
            self.__item[tag] = body
        
        def feed(self, text):
            self.__item = {}
            return super().feed(text)
        
        def getResult(self):
            return self.__item

    def __init__(self, preprocess=None, encoding=None):
        if not preprocess is None and not callable(preprocess):
            raise TypeError('preprocess needs to be a callable!')
        self.subparser = self.SubParser(encoding)
        self.preprocess = preprocess
        super().__init__(self.__class__.regex, encoding)
    
    def handleCapture(self, c):
        item = self.subparser.feed(c.group(2)).getResult();
        text = c.group(3)
        item['TEXT'] = text if self.preprocess is None else self.preprocess(text) 
        self.__docs[c.group(1)] = item
    
    def feed(self, text):
        self.__docs = {}
        return super().feed(text)
    
    def getResult(self):
        return self.__docs


class QueryParser(parser):
    filename = 'queries'
    regex = '<DOC (\d+)>\n([^<]+)\n</DOC>'
    
    class SubParser(parser):
        regex = '([^\n]+)'
        def __init__(self, encoding=None):
            super().__init__(self.__class__.regex, encoding)
        
        def handleCapture(self, c):
            self.__items.append(c.group(1))
        
        def feed(self, text):
            self.__items = []
            return super().feed(text)
        
        def getResult(self):
            return self.__items

    def __init__(self, encoding=None):
        self.subparser = self.SubParser(encoding)
        super().__init__(self.__class__.regex, encoding)
    
    def handleCapture(self, c):
        items = self.subparser.feed(c.group(2)).getResult();
        self.__queries[c.group(1)] = items
    
    def feed(self, text):
        self.__queries = {}
        return super().feed(text)
    
    def getResult(self):
        return self.__queries


class RelationParser(parser):
    filename = 'rels'
    regex = '(\d+) 0 ([\d\w-]+) 1'
    
    def __init__(self, encoding=None):
        super().__init__(self.__class__.regex, encoding)
    
    def handleCapture(self, c):
        def add(_map, _idx, _elem):
            if _idx in _map:
                _map[_idx].append(_elem)
            else:
                _map[_idx] = [_elem]
        
        add(self.__docToQuery, c.group(2), c.group(1))
        add(self.__queryToDoc, c.group(1), c.group(2))
        
    
    def feed(self, text):
        self.__docToQuery = {}
        self.__queryToDoc = {}
        return super().feed(text)
    
    def getResult(self):
        return self.__docToQuery, self.__queryToDoc

