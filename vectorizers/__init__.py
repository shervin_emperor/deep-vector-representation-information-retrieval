from abc import ABC as _ABC, abstractmethod as _abstractmethod
import msgpack as _msgpack
import msgpack_numpy as _msgpack_numpy
import pickle as _pickle
import gzip as _gzip

__all__ = ['Base']

_msgpack_numpy.patch()
_compression = 7

class Base(_ABC):
    def fit(self, X):
        return self
    
    def vectorize(self, X, save_dir=None):
        v = self.vecfn(X)
        if save_dir:
            self.save(v, save_dir)
        return v
    
    def fit_vectorize(self, X, save_dir=None):
        self.fit(X)
        return self.vectorize(X, save_dir)
    
    @_abstractmethod
    def vecfn(self, X):
        pass
    
    @classmethod
    def save(cls, data, diraddr=None):
        try:
            with _gzip.open((diraddr or './') + '{}'.format(cls.name) + '.pkg.gzip', 'wb', _compression) as f:
                _msgpack.pack(data, f, use_bin_type=True)
        except:
            with _gzip.open((diraddr or './') + '{}'.format(cls.name) + '.pkg.gzip', 'wb', _compression) as f:
                _pickle.dump(data, f)
    
    @classmethod
    def load(cls, diraddr=None):
        try:
            with _gzip.open((diraddr or './') + '{}'.format(cls.name) + '.pkg.gzip', 'rb', _compression) as f:
                r = _msgpack.load(f, use_list=False)
        except:
            with _gzip.open((diraddr or './') + '{}'.format(cls.name) + '.pkg.gzip', 'rb', _compression) as f:
                r = _pickle.load(f)
        return r
    
    name = 'Vec'
    seperator = ' '
