import string
import numpy as np
import gzip
import scipy.spatial.distance as spatialdist
from functools import reduce, partialmethod
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize, sent_tokenize

def is_iterable(obj):
    return hasattr(obj, '__iter__')

def is_subscriptable(obj):
    return hasattr(obj, '__getitem__')

def call_if_callable(obj):
    return obj() if callable(obj) else obj

def iter_by_sorted_idx(x, indeces):
    it = iter(x)
    i = 0
    for u in indeces:
        while (i != u):
            i += 1
            next(it)
        yield next(it)

def average_precision(y_true, y_pred):
    sum = 0.0
    p_recall = 0.0
    for l in range(len(y_pred)):
        precision, n_recall = precision_recall(y_true, y_pred[:l+1])
        diff = n_recall - p_recall
        p_recall = n_recall
        sum += diff * precision
    return sum

def precision_recall(y_true, y_pred):
    yt = np.sort(y_true)
    yp = np.sort(y_pred)
    args = searchsorted(yt, yp, np.argsort)
    precision = (yt[args] == yp).sum() / len(y_pred)
    recall = (yt[args] == yp).sum() / len(y_true)
    return precision, recall

def mean_average_precision(y_true, y_pred):
    return np.mean([average_precision(yt, yp) for yt, yp in zip(y_true, y_pred)])

def lexsort(X):
	return np.lexsort(np.transpose(np.reshape(X, (len(X), -1)))[::-1])

def searchsorted(a, v, argsorter=np.argsort, side='left'):
    r = (side == 'right')
    if not callable(argsorter):
        raise TypeError('sorter needs to be callable')
    a_arg = argsorter(a)
    v_arg = argsorter(v)
    pos = np.empty(len(v), dtype=int)
    pos.fill(a_arg[-1])
    a_i = 0
    v_i = 0
    while(True):
        if a_i == len(a_arg) or v_i == len(v_arg):
            break
        a_idx = a_arg[a_i]
        v_idx = v_arg[v_i]
        cnd_arr = [v[v_idx], a[a_idx]]
        if r:
            cnd_arr = cnd_arr[::-1]
        if (argsorter(cnd_arr)[0] == r):
            pos[v_idx] = a_idx
            v_i += 1
        else:
            a_i += 1
    return pos

def searchsubset_sorted(a, v):
    """a and v must be presorted"""
    pos = np.empty(len(v), dtype=int)
    a_i = 0
    v_i = 0
    while(True):
        if v_i == len(v_arg):
            break
        if a_i == len(a_arg):
            raise ValueError('v not a subset of a or entry not presorted!!!')
        a_idx = a_arg[a_i]
        v_idx = v_arg[v_i]
        if (a[a_idx] == v[v_idx]):
            pos[v_idx] = a_idx
            v_i += 1
        else:
            a_i += 1
    return pos

def splitAcc(X, *cuts):
    r = []
    acc = 0
    for l in cuts:
        r.append(X[acc:acc + l])
        acc = acc + l
    r.append(X[acc:])
    return r

def partialclass(cls, *args, **kwargs):

    class NewCls(cls):
        __init__ = partialmethod(cls.__init__, *args, **kwargs)

    return NewCls

class GenWrapper():
    def __init__(self, genFunc):
        self.gFn = genFunc
    
    def __iter__(self):
        return self.gFn()
    
    def __str__(self):
        s = '['
        for e in iter(self):
            s += str(e) + ', '
        s = s[:-2] + ']'
        return s

class TextFile():
    def __init__(self, file_addr):
        self.addr = file_addr + '.gzip'
    
    @classmethod
    def push_unto(cls, push_gen, text):
        next(push_gen)
        return push_gen.send(text)
    
    def push(self):
        f = gzip.open(self.addr, 'wt')
        def g(txt):
            for sent in TextProc.sentTokenize(txt):
                words = TextProc.rmStopWords(TextProc.wordTokenize(sent))
                if(len(words)):
                    yield ' '.join(words)
        try:
            while(1):
                txt = yield
                if txt == -1:
                    f.close()
                    yield None
                    break
                _t = '. '.join(g(txt))
                start = f.tell()
                f.write(_t)
                end = f.tell()
                f.write('\n')
                yield start, end
        finally:
            f.close()
    
    def pull(self):
        with gzip.open(self.addr, 'rt') as f:
            for line in f:
                yield line[:-1]

class TextProc:
    @classmethod
    def wordTokenize(cls, text):
        tokens = word_tokenize(text)
        return tokens
    
    @classmethod
    def sentTokenize(cls, text):
        tokens = sent_tokenize(text)
        return tokens
    
    @classmethod
    def stem(cls, word):
        stemmed = cls.stemmer.stem(word)
        return stemmed
    
    @classmethod
    def rmStopWords(cls, tokenArr):
        filtered = [w for w in tokenArr if w not in cls.stop_words]
        return filtered
    
    @classmethod
    def indexNonStopWords(cls, tokenArr):
        filtered = [idx for idx, w in enumerate(tokenArr) if w not in cls.stop_words]
        return filtered
    
    @classmethod
    def stemText(cls, text, split=False):
        tokens = [cls.stem(w) for w in cls.wordTokenize(text) if w not in cls.stop_words]
        filtered_text = tokens if split else ' '.join(tokens)
        return filtered_text
    
    stemmer = SnowballStemmer('english')
    stop_words = frozenset(stopwords.words('english') + list(string.punctuation) + ['``', '""', "''"])

