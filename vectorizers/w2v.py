from gensim.models import Word2Vec as _Word2Vec, KeyedVectors as _KeyedVectors
from helpers import is_subscriptable as _is_subscriptable, TextProc as _TextProc
from sklearn.preprocessing import StandardScaler as _StandardScaler
from multiprocessing.pool import ThreadPool as _ThreadPool
from . import Base as _Base
from . import _compression as _compression
from numpy import empty as _empty, mean as _mean, zeros as _zeros, sum as _sum, argsort as _argsort

__all__ = ['init',
		   'Vectorizer']

def init(vec_addr, is_normalized=False):
	global _w2v_model
	_w2v_model = _KeyedVectors.load(vec_addr, mmap='r')
	if is_normalized:
		_w2v_model.vectors_norm = _w2v_model.vectors  # prevent recalc of normed vectors


class Vectorizer(_Base):
    def __init__(self, apply_mean=True, n_thread=8):
        self.m = apply_mean
        self.tp = _ThreadPool(n_thread)
    
    def __del__(self):
        self.tp.close()
    
    def __veclen(self):
        return len(_w2v_model.vectors[0])
    
    def vecfn(self, X):
        if _is_subscriptable(X):
            def fn(i, x):
                arr[i] = self.__process(x)
            arr = _empty((len(X), self.__veclen()))
            self.tp.starmap(fn, enumerate(X))
        else:
            arr = self.tp.map(self.__process, X)
        return arr
    
    def __process(self, x):
        _r = []
        for w in _TextProc.wordTokenize(x):
            try:
                _r.append(_w2v_model.get_vector(w))
            except(KeyError):
                pass
        if len(_r):
            _r = _mean(_r, axis=0) if self.m else _sum(_r, axis=0)
        else:
            _r = _zeros(self.__veclen())
        return _r
    
    name = 'W2v'
    seperator = ' '

class IdfVectorizer(_Base):
    def __init__(self, shift_mean=False, apply_mean=True, n_thread=8):
        self.stdScl = _StandardScaler(with_mean=shift_mean)
        self.w2vVec = Vectorizer(apply_mean, n_thread)
    
    def vecfn(self, X):
        return (1.0 if self.stdScl.scale_ is None else self.stdScl.scale_,
                0.0 if self.stdScl.mean_ is None else self.stdScl.mean_)
    
    @classmethod
    def load(cls, diraddr=None):
        v = Vectorizer.load(diraddr)
        scale, mean = super().load(diraddr)
        return (v - mean) / scale
    
    def fit(self, X, y=None):
        self.stdScl.fit(self.w2vVec.vectorize(X))
        return self
    
    def vectorize(self, X, save_dir=None):
        v = self.w2vVec.vectorize(X, save_dir)
        super().vectorize(X, save_dir)
        return self.stdScl.transform(v)
    
    def fit_vectorize(self, X, save_dir=None):
        v = self.w2vVec.vectorize(X, save_dir)
        self.stdScl.fit(v)
        super().vectorize(X, save_addr)
        return self.stdScl.transform(v)
    
    name = 'W2vIdf'

class W2vWordExpander():
    def __init__(self, n_thread=8):
        self.tp = _ThreadPool(n_thread)
    
    def __del__(self):
        self.tp.close()
    
    def expand(self, X, n, n_thread=8):
        return self.tp.map((lambda x: self.__process(x, n)), X)
    
    def __process(self, x, n):
        if n < 1:
            return x
        words = []
        scores = []
        for w in _TextProc.wordTokenize(x):
            try:
                r = _w2v_model.most_similar(w, topn=n)
                words.extend(e[0] for e in r)
                scores.extend(e[1] for e in r)
            except(KeyError):
                pass
        args = _argsort(scores)[-n:]
        expanded = self.seperator.join([x] + [words[i] for i in args])
        return expanded
    
    seperator = ' '