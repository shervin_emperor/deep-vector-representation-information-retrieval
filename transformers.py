import numpy as np
from abc import ABC, abstractmethod
from queue import PriorityQueue
from sklearn.neighbors import NearestNeighbors
from scipy.spatial.distance import cdist
from helpers import iter_by_sorted_idx, is_subscriptable, partialclass
from vectorizers import Base as BaseVectorizer

data_path = './data/'

class NNBase(ABC):

    def fit_transform(self, X, n_neighbors=None):
        return self.fit(X).transform(X, n_neighbors)
    
    @abstractmethod
    def fit(self, X, y=None):
        return self
    
    @abstractmethod
    def transform(self, X, n_neighbors=None):
        pass


class SKNN(NNBase):
    def __init__(self, n_neighbors=5, radius=1.0, 
                 algorithm='auto', leaf_size=30, 
                 metric='minkowski', p=2, 
                 metric_params=None, n_jobs=1, 
                 **kwargs):
        self.nnl = NearestNeighbors(n_neighbors, radius, algorithm, leaf_size, 
                                    metric, p, metric_params, n_jobs, **kwargs)
    
    def fit(self, X, y=None):
        self.nnl.fit(X)
        return super().fit(X, y)
    
    def transform(self, X, n_neighbors=None):
        nn = self.nnl.kneighbors(X=X, n_neighbors=n_neighbors, return_distance=False)
        return nn


class QNN(NNBase):
    def __init__(self, metric='euclidean', n_neighbors=5):
        self.m = metric
        self.n = n_neighbors
    
    def fit(self, X, y=None):
        self.X = X
        return super().fit(X, y)
    
    def transform(self, X, n_neighbors=None):
        class Helper:
            def __init__(self, elem, dist):
                self.e = elem
                self.d = dist
                
            def __lt__(self, other):
                return self.d > other.d

            def get(self):
                return self.e
        
        n = n_neighbors or self.n
        pqs = [PriorityQueue(n + 1) for x in X]
        for i, e in enumerate(self.X):
            dists = cdist([e], X, self.m)[0]
            for j, x in enumerate(X):
                pqs[j].put(Helper(e, dists[j]))
                if pqs[j].full():
                    pqs[j].get()
        
        return [[pqs[j].get().get() for i in range(n)] for j, x in enumerate(X)]


class AliasRpl():
    def fit(self, X, y=None):
        self.X = X
        return self
    
    def transform(self, X):
        if self.X is None:
            return list(X)
        if is_subscriptable(self.X):
            t = [[self.X[e] for e in x] for x in X]
        else:
            u, indices = np.unique([e for x in X for e in x], return_inverse=True)
            items = list(iter_by_sorted_idx(self.X, u))
            it = iter(indices)
            t = [[items[next(it)] for e in x] for x in X]
        return t


class QueryExpander():
    def __init__(self, nnl):
        if issubclass(nnl.__class__, NNBase):
              self.nnl = nnl
        else:
              raise TypeError('nnl should be of base NNBase')
        self.alias = AliasRpl()
    
    def fit(self, X, y=None):
        self.X = X
        self.alias.fit(y)
        self.nnl.fit(X, y)
        return self
    
    def expand(self, X, y=None, n_neighbors=None):
        nn = self.nnl.transform(X, n_neighbors)
        it = iter(X) if y is None else iter(y)
        e = [[next(it)] + l for l in self.alias.transform(nn)]
        return e
    
    def fit_expand(self, X, y):
        return self.fit(X, y).expand(X)


class QuerySearch():
    def __init__(self, nnCls=SKNN):
        if issubclass(nnCls, NNBase):
            self.nn = nnCls
        else:
            raise TypeError('nnCls should be a child class of NNBase')
        self.load = False
        self.n_expand = 0
        self.data_path = data_path
    
    @classmethod
    def __typeCheckVec(cls, vectorizer):
        if issubclass(vectorizer.__class__, BaseVectorizer):
            return True
        else:
            raise TypeError('vectorizer should be derived from vectorizers.Base class')
    
    def __reset_exp(self):
        self.qry = self.raw_qry
        self.n_expand = 0
        self.qe = None
    
    def __reset_srch(self):
        self.qs = None
    
    def __vectorize(self, data, vectorizer, addr=None):
        try:
            if not self.load:
                raise FileNotFoundError
            return vectorizer.load(self.data_path + addr)
        except(FileNotFoundError):
            if data is None:
                raise "data can't be None!"
            return vectorizer.vectorize(data, self.data_path + addr)
    
    def set_params(self, **kwargs):
        for k, v in kwargs.items():
            exec('self.' + k + '= v')
        return self
    
    def setPath(self, path):
        self.data_path = path
        return self
    
    def fit_qry(self, X):
        self.raw_qry = X
        self.__reset_exp()
        self.__reset_srch()
        return self
    
    def fit_doc(self, X, vectorizer, metric='cosine'):
        if self.__typeCheckVec(vectorizer):
            self.sv = vectorizer
            self.sv.fit(X)
        self.d = self.__vectorize(X, self.sv, 'docs_')
        self.snearest = SKNN(metric=metric).fit(self.d)
        self.smetric = metric
        self.__reset_srch()
        return self
    
    def fit_exp(self, X, vectorizer, metric='cosine'):
        if self.__typeCheckVec(vectorizer):
            self.ev = vectorizer
            self.ev.fit(X)
        self.e = self.__vectorize(X, self.ev, 'exp_')
        self.qexpander = QueryExpander(SKNN(metric=metric)).fit(self.e, X)
        self.emetric = metric
        self.__reset_exp()
        return self
    
    def shouldLoad(self, load=True):
        self.load = load
        return self
    
    def expand(self, n_expand):
        if self.qe is None:
            self.qe = self.__vectorize(self.raw_qry, self.ev, 'qry_')
        elif self.n_expand == n_expand:
            return self
        if n_expand < 1:
            self.qry = self.raw_qry
            self.n_expand = 0
        else:
            _nn = self.qexpander.expand(self.qe, self.raw_qry, n_expand)
            self.qry = [self.ev.__class__.seperator.join(query) for query in _nn]
            self.n_expand = n_expand
        self.__reset_srch()
        return self
    
    def search(self, n_search):
        qry_name = '_'.join((['exp', self.ev.__class__.name, self.emetric[:3], str(self.n_expand)]
                             if self.n_expand != 0 else []) + ['qry_'])
        if self.qs is None:
            self.qs = self.__vectorize(self.qry, self.sv, qry_name)
        return self.snearest.transform(self.qs, n_search)

