from . import Base as _Base
from sklearn.feature_extraction.text import TfidfVectorizer as _TfidfVectorizer

__all__ = ['Vectorizer']

class Vectorizer(_Base):
    def __init__(self, *args, **kwargs):
        self.tfidf_vectorizer = _TfidfVectorizer(*args, **kwargs)
    def fit(self, X, y=None):
        self.tfidf_vectorizer.fit(X, y)
        return self
    def vecfn(self, X):
        return self.tfidf_vectorizer.transform(X)
    def fit_vectorize(self, X, save_dir=None):
        v = self.tfidf_vectorizer.fit_transform(X)
        if save_dir:
            self.save(v, save_dir)
        return v
    
    name = 'Tfidf'
    seperator = ' '